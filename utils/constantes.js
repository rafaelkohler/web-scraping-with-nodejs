  BASE_URL = "https://gamefaqs.gamespot.com",

  browserHeaders = {
    accept:
      "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
    "cache-control": "max-age=0",
    cookie:
      'gf_dvi=ZjYwMDBjMDAxMDA5NThkMDM0ZGYwZjlkYmZhY2VmOGNkYThjYWExMjEzZTNjNTMwY2EyYzUzOGE5MWM5NjAwMGMwMDE%3D; gf_geo=MjAwMToxMjg0OmYwMTY6NWQ4YzpmNTM2OjEzMTg6ZWY4NTplMThjOjc2OjI5NzA%3D; fv20210115=1; dfpsess=j; __utma=132345752.1362564590.1610661890.1610661890.1610661890.1; __utmz=132345752.1610661890.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmc=132345752; __utmb=132345752.0.10.1610661890; tglr_tenant_id=src_1kYs5kGF0gH8ObQlZU8ldA7KFYZ; tglr_sess_id=f99cf9f8-4bef-4b85-a63f-e86b7738f285; tglr_req=https://gamefaqs.gamespot.com/n64/catgory/99-all; tglr_sess_count=1; tglr_anon_id=2813a20c-adc1-4852-89c5-b8490846fc73; s_vnum=1613253890618%26vn%3D1; s_invisit=true; s_lv_gamefaqs_s=First%20Visit; cohsn_xs_id=87319d26-e164-4147-b34a-36fabf8e5dfb; AMCVS_10D31225525FF5790A490D4D%40AdobeOrg=1; s_vi=[CS]v1|30006001D457E4CB-400008C8E3314B34[CE]; s_ecid=MCMID%7C62693745692631699594522162879969716854; AMCV_10D31225525FF5790A490D4D%40AdobeOrg=1585540135%7CMCIDTS%7C18642%7CMCMID%7C62693745692631699594522162879969716854%7CMCAAMLH-1611266691%7C4%7CMCAAMB-1611266691%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1610669091s%7CNONE%7CMCAID%7C30006001D457E4CB-400008C8E3314B34%7CvVersion%7C4.4.0; s_cc=true; aamgam=segid%3D12477927; aam_uuid=62934286574555803834509926076937842446; spt=no; __gads=ID=967833d07c0228c1:T=1610661894:S=ALNI_MaYLA4jLzPDzVOdliuM_kTr4Ws2jw; tglr_ref=null; __qca=P0-25113049-1610661994436; s_sq=%5B%5BB%5D%5D; RT="z=1&dm=gamespot.com&si=68mj01mwoe7&ss=kjxei3rn&sl=0&tt=0"; OptanonConsent=isIABGlobal=false&datestamp=Thu+Jan+14+2021+19%3A08%3A05+GMT-0300+(Hor%C3%A1rio+Padr%C3%A3o+de+Bras%C3%ADlia)&version=6.7.0&hosts=&consentId=8e1d8f0b-101d-4caa-b07a-82d275351083&interactionCount=1&landingPath=NotLandingPage&groups=C0002%3A1%2CC0003%3A1%2CC0004%3A1%2CC0005%3A1&AwaitingReconsent=false&geolocation=BR%3BPR; OptanonAlertBoxClosed=2021-01-14T22:08:05.315Z; s_getNewRepeat=1610662085350-New; s_lv_gamefaqs=1610662085351; QSI_HistorySession=https%3A%2F%2Fgamefaqs.gamespot.com%2Fn64%2Fcatgory%2F99-all~1610661894343%7Chttps%3A%2F%2Fgamefaqs.gamespot.com%2Fn64%2Fcatgory%2F999-all~1610661897745%7Chttps%3A%2F%2Fgamefaqs.gamespot.com%2Fn64%2Fcategory%2F999-all~1610661988449%7Chttps%3A%2F%2Fgamefaqs.gamespot.com%2Fn64%2Fcategory%2F999-all%3Fpage%3D1~1610662012149%7Chttps%3A%2F%2Fgamefaqs.gamespot.com%2Fn64%2Fcategory%2F999-all%3Fpage%3D0~1610662087728; RT="z=1&dm=gamefaqs.gamespot.com&si=5479acde-9030-42b2-afeb-1b17b5a4fe7f&ss=kjxei3rn&sl=4&tt=7l7&ul=647p&bcn=%2F%2Fb855d723.akstat.io%2F"',
    "sec-fetch-dest": "document",
    "sec-fetch-mode": "navigate",
    "sec-fetch-site": "none",
    "sec-fetch-user": "?1",
    "upgrade-insecure-requests": "1",
    "user-agent":
      "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36",
  }

module.exports = {
    BASE_URL: BASE_URL,
    browserHeaders: browserHeaders
}
