const fs = require("fs");

writeToFile = (data, filename) => {
    const promiseCallback = (resolve, reject) => {
      fs.writeFile(filename, data, (error) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(true);
      });
    };
    return new Promise(promiseCallback);
  };
  
  readFromFile = (filename) => {
    const promiseCallback = (resolve, reject) => {
      fs.readFile(filename, "utf-8", (error, contents) => {
        if (error) {
          resolve(null);
          return;
        }
        resolve(contents);
      });
    };
    return new Promise(promiseCallback);
  };
  
  saveData = (data, path) => {
      const promiseCallback = async (resolve, reject) => {
          if (!data || data.length === 0) return resolve(true);
  
          const dataToStore = JSON.stringify({data: data}, null, 2);
          const created = await writeToFile(dataToStore, path)
          resolve(true);
      };
  
      return new Promise(promiseCallback);
  }

  module.exports = {
    writeToFile: writeToFile,
    readFromFile: readFromFile,
    saveData: saveData
}